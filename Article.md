# (not so) Secret Notifications

# Stalkerware and its Repercussions

“In 2019, the U.S.-based National Network to End Domestic Violence found that 71% of domestic abusers monitor survivors’ computer activities, while 54% tracked survivors’ cell phones with stalkerware” (Hansen, 2021). As ethical developers, we have a duty to ensure the safety of our end users to the best of our ability. This blog will present a holistic representation of the stalkerware threat followed by a walk-through Android tutorial on what we, as developers, can do to safeguard our end users privacy against stalkerware. Notably, we will focus on notification safety due to their usage across all applications posing a high risk surveillance threat.

![(not%20so)%20Secret%20Notifications%2038a361fdd37f4f9d83ef39f326042c58/Stalkerware_News_Images_(1).png]((not%20so)%20Secret%20Notifications%2038a361fdd37f4f9d83ef39f326042c58/Stalkerware_News_Images_(1).png)

"Stalkerware technology is used in an intimate relationship to conduct powerfully intrusive covert or coerced surveillance of an intimate or former partner’s mobile device without their knowledge." (Khoo et al. 2019) Stalkerware collects all manner of information from a targeted individual (the survivor), including highly sensitive personally identifiable information (PII), inherently compromising one's privacy. A superficial search of GitHub reveals many stalkerware (see [1](https://github.com/patanjalikr13/Android-Undetectable-Spy-App), [2](https://github.com/ranaaditya/SPYZIER-APP), [3](https://github.com/TheoLong/Android_spyware)) and there are even hundred of such applications available to consumers for a small fee (Khoo et al. 2019).

While mature stalkerware is not limited to monitoring notifications, the sensitive PII that can be collected from notifications alone encompasses that of private conversations, emails, banking information, username and passwords, incoming calls, etc.. In turn, targeted individuals are controlled, isolated, and manipulated by the operator of the stalkerware (the abuser) through the application's disclosure of the target's sensitive PII in real time.

## Developer Scenario

> How do we, as ethical developers, work to safeguard our users notification privacy?

If you are creating a new application and you are committed to the safety of your end users, specifically domestic abuse survivors, it is important to understand *how* stalkerware actually takes advantage of privacy vulnerabilities to monitor, collect, and share notification data with the operators. Especially when developing for vulnerable communities, developers must utilize privacy by design methodologies. To help you do this, our tutorial instructs you in the nuances of notification surveillance, so you can be aware of what you are up against, and possible solutions for your design.

## Systematic Analysis of Privacy Requirements and Threats

How much of a threat does stalkerware represent? Take a look at our [NIST Score Card](https://nvd.nist.gov/vuln-metrics/cvss/v3-calculator?vector=AV:P/AC:L/PR:H/UI:N/S:C/C:H/I:L/A:N/E:H/RL:W/RC:C&version=3.1) to explore the different dimensions of this threat before you continue onto the decomposition of notification surveillance through the threat model (Stallings, 2019).

![(not%20so)%20Secret%20Notifications%2038a361fdd37f4f9d83ef39f326042c58/Untitled.png]((not%20so)%20Secret%20Notifications%2038a361fdd37f4f9d83ef39f326042c58/Untitled.png)

NIST score for stalkerware

Below, we break down the three key privacy concepts: (1) the vulnerability, (2) the threat, and (3) the action (Stallings, 2019).

**The privacy vulnerability:** The vulnerability arises from the android OS framework, specifically the notification listener service.

The android application framework allows a stalkerware application to access all notifications published on the device. This functionality requires a permission which must be granted manually by the user. Once this permission has been granted, the application can access notification content in the background while remaining completely invisible to the user.

Granting this permission requires user interaction, first by accessing the Notification Access settings:

![(not%20so)%20Secret%20Notifications%2038a361fdd37f4f9d83ef39f326042c58/Untitled_1.jpeg]((not%20so)%20Secret%20Notifications%2038a361fdd37f4f9d83ef39f326042c58/Untitled_1.jpeg)

While the following warning is presented, this is not enough to prevent privacy breaches:

![(not%20so)%20Secret%20Notifications%2038a361fdd37f4f9d83ef39f326042c58/Untitled_2.jpeg]((not%20so)%20Secret%20Notifications%2038a361fdd37f4f9d83ef39f326042c58/Untitled_2.jpeg)

**The privacy threat:** The root threat is surveillance (either covert or coerced) of a mobile device resulting from a threat of intrusion, specifically to record all notification data. 

Following the NIST threat model, the resulting harms from this privacy threat include loss of self-determination (an abuser uses the obtained information from the stalkerware to cause a loss of autonomy for the survivor who begins to act within self-imposed restrictions out of fear), discrimination (an abuser can create a power imbalance through taking advantage of the gathered data from the stalkerware and exercise their knowledge to control, isolate, etc. the survivor), and loss of trust (if a survivor knows about the stalkerware but cannot remove it for fear of retribution from the abuser, then the survivor will lose trust in utilizing their mobile device, thus becoming more isolated and possibly fearing the security of using mobile devices once they are safe) (NISTIR 8026) (Stallings, 2019).

And according to the NIST *Guide for Conducting Risk Assessments* (SP 300-30), we categorize the aforementioned threats' sources to be two-fold: adversarial (the abuser/operator seeking to exploit their partner's dependence on their mobile device) and structural (the vulnerability of the android application framework) (Stallings, 2019). 

**The threat action:** downloading/installing the stalkerware and granting the associated permissions, specifically the 'Notification Access' permission.

Now that the threat is fully understood, it is imperative to analyze stalkerware within a legal framework.  Due to the nature of international law and the diversity in privacy law by country, we choose to focus our investigations on the context of Canada's national laws and their *Personal Information Protection and Electronic Documents Act* (PIPEDA). Additionally, it is worth noting that stalkerware within international and national law is an emerging issue with few legal precedence and little research on how to be dealt with. The report released by Khoo et al. in 2019 seems to be the first major research conducted on this manner of malicious software activity in Canada and one of the first in the world.

In summary, the report presents:

- In Canadian context, the "legality of the creation, sale, and use of consumer-level spyware apps has not yet been closely considered" (p. 1).
- Survivors targeted by stalkerware 'may bring a cause of action (lawsuit)' against the individual who initiated the surveillance by stalkerware through a wrongful act on grounds of "invasion of privacy, public disclosure of private facts, breach of confidence, and intentional infliction of of mental suffering" (p. 2).
- PIPEDA, although it contains provisions ensuring [informed consent](https://www.priv.gc.ca/en/privacy-topics/privacy-laws-in-canada/the-personal-information-protection-and-electronic-documents-act-pipeda/p_principle/principles/p_consent/), notice (for informing the users of a software), and [identifying purposes](https://www.priv.gc.ca/en/privacy-topics/privacy-laws-in-canada/the-personal-information-protection-and-electronic-documents-act-pipeda/p_principle/principles/p_purposes/), there some key loopholes affording stalkerware companies gray areas in which to operate. Such as:
    1. [Section 4(2)(b)](https://laws-lois.justice.gc.ca/ENG/ACTS/P-8.6/page-1.html) states that individuals collecting, using, or disclosing PII for "personal or domestic purposes" are excluded from the *Act*. This then means that to prosecute the operator, one needs to pursue criminal or civil law. This then confronts an age old issue of holding abusers accountable by law and the struggle for survivors to come forward in the first place.
    2. In a situation where a stalkerware is able to disclose the targeted person's information directly to the operator without disclosing the information to the company, then the company is *not* accountable to PIPEDA. This can prove quite challenging to enforce.
    3. PIPEDA does not require for informed consent of third parties' data being collected, but rather there is 'implied consent of third parties'. Due to the reality for victims affected, the second party is the operator of the stalkerware and the third party is the targeted individual.  Because of this, the boundaries set forth by PIPEDA for informed consent are vague and it can be argued that the stalkerware company are not, in fact, responsible for obtaining the consent to collect, use, or disclose the data of the targeted individuals. 
- It is also worth noting that the Office of the Privacy Commissioner of Canada found that "granting an application permission to have the capability to access their personal information" is **not** the same as providing informed consent to the application to collect this information [(PIPEDA Report of Findings, 2014)](https://www.priv.gc.ca/en/opc-actions-and-decisions/investigations/investigations-into-businesses/2014/pipeda-2014-008/).
- Additionally, stalkerware is considered to be dual-use technology (one that may be intended as benevolent, such as child monitoring apps for parents, but can be utilized maliciously).  This makes it even more difficult to enforce accountability of stalkerware companies.

Because of these pitfalls, when the privacy risks are exploited by stalkerware operators, it is extremely difficult to prosecute the offenders. As developers, it is time to step up and take action to protect the privacy of our end users. 

# Notification Privacy Tutorial

To demonstrate stalkerware capabilities and operations concretely, we will inspect how two applications interact indirectly through the Android application framework:

- *notifier_app*: This sample application will be used to send notifications and demonstrate a mitigation strategy against stalkerware operations. It represents the point of views of ethical developers and end users.
- *listener_app*: This sample application will be used to simulate stalkerware and will be used as a proof of concept for notification surveillance. Our attempts to foil stalkerware will target this application.

## Notifying Users

In order to fully understand the development context, let us detail how an application would publish a Notification for the user. You can follow along by building the [notifier_app](https://gitlab.cs.mcgill.ca/lturpi1/599-a1/-/tree/master/notifier_app).

This tutorial assumes functional knowledge of Android Studio and familiarity with Java Android application structure.

In this example, we create a simple Activity which allows us to send a notification at the press of a button. Within this activity, we can start by writing the method for creating a channel where  the notifications will be attached.

```java
private void createNotificationChannel() {
    // Create the NotificationChannel, but only on API 26+ because
    // the NotificationChannel class is new and not in the support library
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        CharSequence name = getString(R.string.channel_name);
        String description = getString(R.string.channel_description);
        int importance = NotificationManager.IMPORTANCE_DEFAULT;
        NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
        channel.setDescription(description);
        // Register the channel with the system; you can't change the importance
        // or other notification behaviors after this
        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);
    }
}
```

With the channel created, we can create a notification object containing our personalized content which a notification manager will notify the user with:

```java
NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
        .setSmallIcon(R.drawable.ic_baseline_child_care_24)
        .setContentTitle("New Notification")
        .setContentText("Our personalized content!")
        .setPriority(NotificationCompat.PRIORITY_DEFAULT);

NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
notificationManager.notify(100, builder.build());
```

Bringing it all together into a friendly user interface, the complete class of MainActivity for sending notifications looks as follows:

```java
public class MainActivity extends AppCompatActivity {
    private static final String CHANNEL_ID = "testChannel";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createNotificationChannel();

        Button buttonShowNotification = findViewById(R.id.button);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_baseline_child_care_24)
                .setContentTitle("New Notification")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        buttonShowNotification.setOnClickListener(v -> {
            String message = getMessage(v);
            builder.setContentText(message);
            notificationManager.notify(100, builder.build());
        });
    }

    public String getMessage(View view) {
        EditText editText = (EditText) findViewById(R.id.editText);
        String message = editText.getText().toString();
        return message;
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
```

## Listening for Notifications

Now, we will demonstrate the APIs and functionality required to achieve notification surveillance. You can follow along by building the [listener_app](https://gitlab.cs.mcgill.ca/lturpi1/599-a1/-/tree/master/listener_app). Here we also wish to note that our *listener_app* is a modified template from [Chagall/notification-listener-service-example.](https://github.com/Chagall/notification-listener-service-example)

Capturing notification content on an Android device requires using a [background service](https://developer.android.com/guide/components/services) which is done in two parts. First, we must define the service in the application's [Manifest](https://gitlab.cs.mcgill.ca/lturpi1/599-a1/-/blob/master/listener_app/app/src/main/AndroidManifest.xml#L19):

```xml
<service android:name=".NotificationListenerExampleService"
    android:label="@string/service_label"
    android:permission="android.permission.BIND_NOTIFICATION_LISTENER_SERVICE">
    <intent-filter>
        <action android:name="android.service.notification.NotificationListenerService" />
    </intent-filter>
</service>
```

The `android:permission` attribute is not sufficient to grant our application notification access. Our users will still need to manually grant the permission to *listener_app*  as outlined in **Privacy vulnerability** section above.

The second part is to define the implementation structure of the service by extending the `NotificationListenerService` abstract class ([reference](https://developer.android.com/reference/android/service/notification/NotificationListenerService)):

```java
public class NotificationListenerExampleService extends NotificationListenerService {

    public static final String INTENT_KEY = "[...]";
    public static final String NOTIFICATION_CONTENT_KEY = "notification_content";

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    public void onListenerConnected() {
        super.onListenerConnected();
        publishContentForNotification();
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        publishContentForNotification();
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        publishContentForNotification();
    }

    /**
     * Select the most recent notification and publish its content to the MainActivity for display
     */
    private void publishContentForNotification() {
        StatusBarNotification[] activeNotifications = getActiveNotifications();
        if (activeNotifications == null || activeNotifications.length == 0) {
            return;
        }
        Notification lastPostedNotification = activeNotifications[0].getNotification();
        String notifContent = parseContentFromNotification(lastPostedNotification);

        Intent intent = new Intent(INTENT_KEY);
        intent.putExtra(NOTIFICATION_CONTENT_KEY, notifContent);
        sendBroadcast(intent);
    }
    // [...]
}
```

Our [concrete implementation](https://gitlab.cs.mcgill.ca/lturpi1/599-a1/-/blob/master/listener_app/app/src/main/java/com/github/chagall/notificationlistenerexample/NotificationListenerExampleService.java) of NotificationListenerService allows us to handle the `onNotificationPosted` event and retrieve active notifications at any time using `getActiveNotifications`. In the context of this example, only the most recent notification's content is extracted although this could easily be extended to allow surveillance of new notifications.

After retrieving notifications, we can use the [following algorithm](https://gitlab.cs.mcgill.ca/lturpi1/599-a1/-/blob/master/listener_app/app/src/main/java/com/github/chagall/notificationlistenerexample/NotificationListenerExampleService.java#L82) to extract content from them:

```java
    /**
     * Extracts text content from a notification
     *
     * Attempts to do so by extracting `extras` inserted by the NotificationBuilder built-in.
     * In the future, we could also directly inspect the Views in the `Notification#contentView`
     * @param notif Notification to parse content from
     * @return Concatenated text content of the notification
     */
    private String parseContentFromNotification(Notification notif) {
        ExtrasStringBuilder result = new ExtrasStringBuilder();

        String title = notif.extras.getString(Notification.EXTRA_TITLE);
        String titleBig = notif.extras.getString(Notification.EXTRA_TITLE_BIG);
        String[] people = notif.extras.getStringArray(Notification.EXTRA_PEOPLE);
        CharSequence bigText = notif.extras.getCharSequence(Notification.EXTRA_BIG_TEXT);
        CharSequence text = notif.extras.getCharSequence(Notification.EXTRA_TEXT);
        CharSequence[] textLines = notif.extras.getCharSequenceArray(Notification.EXTRA_TEXT_LINES);
        CharSequence subText = notif.extras.getCharSequence(Notification.EXTRA_SUB_TEXT);
        CharSequence infoText = notif.extras.getCharSequence(Notification.EXTRA_INFO_TEXT);
        CharSequence summaryText = notif.extras.getCharSequence(Notification.EXTRA_SUMMARY_TEXT);
        result.addLine(title);
        result.addLine(titleBig);
        result.addLines(people);
        result.addLine(bigText);
        result.addLine(text);
        result.addLines(textLines);
        result.addLine(subText);
        result.addLine(infoText);
        result.addLine(summaryText);

        return result.toString();
    }
```

The `notif.extras` attribute is a serializable key-value store (see [Bundle](https://developer.android.com/reference/android/os/Bundle)) which stores all of the Notification's properties. We can extract content directly using keys defined as `Notification.EXTRA_*` constants. These properties are populated by applications posting notifications using the [Notification.Builder](https://developer.android.com/reference/android/app/Notification.Builder) to display content to the user. In our previous example with the *notifier_app*, the `message` passed to `builder.setContentText` would be captured by our service.

## Putting it Together

From the *notifier_app*, we can now send a notification:

![(not%20so)%20Secret%20Notifications%2038a361fdd37f4f9d83ef39f326042c58/Untitled_3.jpeg]((not%20so)%20Secret%20Notifications%2038a361fdd37f4f9d83ef39f326042c58/Untitled_3.jpeg)

Which will then be intercepted (and displayed in our case) by the `NotificationListenerService` in the *listener_app*.

![(not%20so)%20Secret%20Notifications%2038a361fdd37f4f9d83ef39f326042c58/Untitled_4.jpeg]((not%20so)%20Secret%20Notifications%2038a361fdd37f4f9d83ef39f326042c58/Untitled_4.jpeg)

# Privacy Controls

Circling back to our original question:

> How do we, as ethical developers, work to safeguard our users notification privacy?

## Mitigation Strategy

From the context of the *notifier_app*, we first want to explore the possibilities to mitigate the effect of the *listener_app* on a device. Luckily, the Android application framework does allow us to search for *other* applications listening for notifications on a device. Through this feature, we can help users mitigate malicious intent by **displaying to the end user what packages and their associated applications have been approved to listen to their notifications**. Our solution involves querying the device's system settings to detect applications with notification access as follows:

```java
    private static final String ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";

    /**
     * Detect applications with notification access
     * @return List of package names granted notification access
     */
    private List<String> detectAllListeners() {
        List<String> detectedPackages = new ArrayList<>();
        final String flat = Settings.Secure.getString(getContentResolver(), ENABLED_NOTIFICATION_LISTENERS);
        if (!TextUtils.isEmpty(flat)) {
						// parse the list of packages from the string
            final String[] names = flat.split(":");
            for (String name : names) {
                final ComponentName cn = ComponentName.unflattenFromString(name);
                if (cn != null) {
                    detectedPackages.add(cn.getPackageName());
                }
            }
        }
        return detectedPackages;
    }
```

Once the list of packages has been parsed, we can present it to the user for review. We introduced a new [ListenerDetectionActivity](https://gitlab.cs.mcgill.ca/lturpi1/599-a1/-/blob/master/notifier_app/app/src/main/java/com/example/notifier_app/ListenerDetectionActivity.java) to the *notifier_app* for such a purpose. Note that querying an application's human-readable label requires the `QUERY_ALL_PACKAGES` permission.

```java
public class ListenerDetectionActivity extends AppCompatActivity {
    private static final String ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";
    private static final String ACTION_NOTIFICATION_LISTENER_SETTINGS = "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listener_detection);

        Button button = findViewById(R.id.go_to_settings);
        button.setOnClickListener(v -> {
            startActivity(new Intent(ACTION_NOTIFICATION_LISTENER_SETTINGS));
        });

        RecyclerView listenerRecycler = findViewById(R.id.listener_list);
        listenerRecycler.setLayoutManager(new LinearLayoutManager(this));
        List<String> detectedListeners = detectAllListeners();
        populateRecyclerView(listenerRecycler, detectedListeners);
    }

    private void populateRecyclerView(RecyclerView recycler, List<String> detectedListeners) {
        Adapter adapter = new Adapter(detectedListeners, this.getPackageManager());
        recycler.setAdapter(adapter);
    }

    /**
     * Detect applications with notification access
     * @return List of package names granted notification access
     */
    private List<String> detectAllListeners() {
        List<String> detectedPackages = new ArrayList<>();
        final String flat = Settings.Secure.getString(getContentResolver(),
                ENABLED_NOTIFICATION_LISTENERS);
        if (!TextUtils.isEmpty(flat)) {
            final String[] names = flat.split(":");
            for (String name : names) {
                final ComponentName cn = ComponentName.unflattenFromString(name);
                if (cn != null) {
                    detectedPackages.add(cn.getPackageName());
                }
            }
        }
        return detectedPackages;
    }

	  /**
     * @see androidx.recyclerview.widget.RecyclerView.ViewHolder
     */
    private static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView packageNameView;
        private final TextView packageLabelView;
        private final PackageManager pm;

        public ViewHolder(@NonNull View itemView, PackageManager pm) {
            super(itemView);
            packageNameView = itemView.findViewById(R.id.package_name);
            packageLabelView = itemView.findViewById(R.id.package_label);
            this.pm = pm;
        }

        public void populate(String packageName) {
            TextView textView = itemView.findViewById(R.id.package_name);
            textView.setText(packageName);

            try {
                ApplicationInfo app = pm.getApplicationInfo(packageName, 0);
                String name = pm.getApplicationLabel(app).toString();
                packageLabelView.setText(name);
                packageNameView.setText(packageName);
            } catch (PackageManager.NameNotFoundException e){
                e.printStackTrace();
            }
        }
    }

   /**
    * @see androidx.recyclerview.widget.RecyclerView.Adapter
    */
    private static class Adapter extends RecyclerView.Adapter<ViewHolder> {

        private final List<String> packages;
        private final PackageManager pm;

        private Adapter(List<String> packages, PackageManager pm) {
            this.packages = packages;
            this.pm = pm;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.detected_listener_item, parent, false);
            return new ViewHolder(view, pm);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            String packageName = packages.get(position);
            holder.populate(packageName);
        }

        @Override
        public int getItemCount() {
            return packages.size();
        }
    }
}
```

Here's what it looks like in action:

![(not%20so)%20Secret%20Notifications%2038a361fdd37f4f9d83ef39f326042c58/resized-image-Promo.jpeg]((not%20so)%20Secret%20Notifications%2038a361fdd37f4f9d83ef39f326042c58/resized-image-Promo.jpeg)

It is worth noting that the mitigation is dependent on the Android system capabilities where if the Android framework were to change, then our solution may no longer be functional. Additionally, our solution shifts the onus onto the end user to take initiative to use our application and willingly venture into their settings to find the nefarious applications and turn off their notification access permissions.

However, our application could perform meaningful additional processing on the list of packages. For example, we could cross-reference package names with a database of known malware, validate the application is registered on a legitimate store or filter them based on publisher information. Our application could also perform this detection automatically or even periodically to protect users in an active manner.

## Addressing the Permission Model

Given the constraints of the Android application framework, at this moment, we are limited by our resources to detecting potential stalkerware and supporting users in mitigating its effects. This is not a complete solution and is ultimately dependent on the end user's motivation. For these reasons, we present a list of recommendations that could be implemented at the operating system level to fix the problem at its core. We believe our *listener_app* highlights important flaws in the Android OS permission model. Fixing these flaws is the only way to programmatically prevent stalkerware operation. 

Consumer applications should never be allowed to listen for notifications. The impacts of stalkerware greatly outweigh any legitimate use case by consumer applications. By removing this capability, we can greatly limit the privacy threats of these malicious applications. 

Furthermore, researchers have already expressed recommendations for changes in the permission model which are still valid today. For example, Xu et al. (2016) present various flaws in the operating system's security, including recommendations and open problems in the permission model. Specifically, they challenge "the implied assumption [...] that the end user is the final authority for making permission granting decisions" (Xu et al., 2016). Stalkerware directly takes advantage of this assumption, reiterating the researchers point. As long as we have not solved the problems behind this assumption - the permission model - we can only hope to preserve privacy by restricting control over sensitive information such as by removing notification access from consumer applications.

## Recommendations and Best Practices

While we weren't able to actively prevent notification surveillance, we offer a mitigation strategy and recommendations for Android's permission model. To conclude this post, we'd like to present some best practices in dealing with the stalkerware threat:

As an ethical developer,

- Avoid putting sensitive information in notifications.
- Be clear and transparent when wording privacy and permission statements.
- Keep in mind the needs of your end user by employing techniques such as human-centered design and privacy by design.

As a lawmaker,

- Address and remove gray areas in which stalkerware companies are allowed to operate in.
- Update PIPEDA to protect individuals targeted by abuse. This is especially important as the pitfalls of PIPEDA leave prosecution of abusers to the jurisdiction of other courts of law, namely as criminal and civil law. This has historically been proven to be more challenging due to systemic issues working against survivors, including sexism, racism, and sustained mental health issues.

And finally as a user,

- Be mindful of what permissions you are granting and what applications you are downloading.
- Periodically check your system's permission settings.
- If you are worried you might be in an unsafe situation, please visit [here](https://www.canada.ca/en/public-health/services/health-promotion/stop-family-violence/services.html) for more resources.

# References

1. Cynthia Khoo, Kate Robertson, and Ronald Deibert. 2019. Installing Fear: A Canadian
Legal and Policy Analysis of Using, Developing, and Selling Smartphone
Spyware and Stalkerware Applications. Citizen Lab Research Report No. 120,
University of Toronto. (June 2019).
2. Paige Hanson. 2021. It's Data Privacy Day and Education is Power. CSR Wire. [https://www.csrwire.com/press_releases/717921-its-data-privacy-day-and-education-power](https://www.csrwire.com/press_releases/717921-its-data-privacy-day-and-education-power)
3. William Stallings. 2019. Information Privacy Engineering and Privacy by Design: Understanding Privacy Threats, Technology, and Regulations Based on Standards and Best Practice. Addison-Wesley Professional. 
4. Meng Xu, Chengyu Song, Yang Ji, Ming-Wei Shih, Kangjie Lu, Cong Zheng, Ruian Duan, Yeongjin Jang, Byoungyoung Lee, Chenxiong Qian, Sangho Lee, and Taesoo Kim. 2016. Toward engineering a secure android ecosystem: A survey of existing techniques. ACM Comput. Surv. 49, 2, Article 38 (August 2016), 47 pages. DOI: [http://dx.doi.org/10.1145/2963145](http://dx.doi.org/10.1145/2963145)